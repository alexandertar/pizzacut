//
//  PCAppDelegate.h
//  PizzaCut
//
//  Created by Alexander Taraymovich on 09/08/2014.
//
//

#import <UIKit/UIKit.h>

@interface PCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
