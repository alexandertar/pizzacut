//
//  PCViewController.h
//  PizzaCut
//
//  Created by Alexander Taraymovich on 09/08/2014.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ASValueTrackingSlider.h"
#import <CoreMotion/CoreMotion.h>
#import <iAd/iAd.h>
#import "GADBannerView.h"

@interface PCViewController : UIViewController<GADBannerViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *imagePreview;

@property (nonatomic, strong) IBOutlet ASValueTrackingSlider *slider;

@property (nonatomic, strong) IBOutlet GADBannerView *adBanner;

@end
