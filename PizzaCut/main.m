//
//  main.m
//  PizzaCut
//
//  Created by Alexander Taraymovich on 09/08/2014.
//
//

#import <UIKit/UIKit.h>

#import "PCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PCAppDelegate class]));
    }
}
