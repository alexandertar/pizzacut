//
//  PCViewController.m
//  PizzaCut
//
//  Created by Alexander Taraymovich on 09/08/2014.
//
//

#import "PCViewController.h"

@interface PCViewController ()

@end

@implementation PCViewController

@synthesize imagePreview;
@synthesize slider;
@synthesize adBanner;

int oldValue = 1;
NSString *kSliceLayerName = @"sliceLayer";
NSString *kCircleLayerName = @"circleLayer";

CALayer *viewLayer;
CALayer *sliceLayer;

CGFloat width;
CGFloat height;

CGFloat yaw;

CMMotionManager *motionManager;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
//    imagePreview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height - 120)];
	
	
	//----- SHOW LIVE CAMERA PREVIEW -----
	AVCaptureSession *session = [[AVCaptureSession alloc] init];
	session.sessionPreset = AVCaptureSessionPresetMedium;
	
	AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
	
	captureVideoPreviewLayer.frame = self.imagePreview.bounds;
	[self.imagePreview.layer addSublayer:captureVideoPreviewLayer];
	
	AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	
	NSError *error = nil;
	AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
	if (!input) {
		// Handle the error appropriately.
		NSLog(@"ERROR: trying to open camera: %@", error);
	}
	[session addInput:input];
	
	[session startRunning];
    
	viewLayer = self.imagePreview.layer;
    
    [[[self view] layer] addSublayer:viewLayer];
    
//    slider = [[ASValueTrackingSlider alloc] initWithFrame:CGRectMake(20, self.view.bounds.size.height - 120, 280, 120)];
//    slider.minimumValue = 1;
//    slider.maximumValue = 10;
//    slider.value = 6;
    [self.slider setMaxFractionDigitsDisplayed:0];
    [slider addTarget:self
                 action:@selector(sliderValueChanged:)
       forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:slider];
    
    motionManager = [[CMMotionManager alloc] init];
    
    motionManager.gyroUpdateInterval = 1.0/60.0;
    [motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMDeviceMotion *motion, NSError *error) {
        [self updateCircle:motion.attitude];
    }];
    
    adBanner.adUnitID = @"ca-app-pub-4068597968074785/427689195";
    adBanner.rootViewController = self;
    
    // Initiate a generic request to load it with an ad.
    [adBanner loadRequest:[self request]];
}

-(void)updateCircle:(CMAttitude*)attitude {
    yaw = attitude.yaw;
    
    CGRect baseRect = imagePreview.frame;
    
    CALayer *compositeMaskLayer = [CALayer layer];
    [compositeMaskLayer setFrame:baseRect];
    [compositeMaskLayer setPosition:CGPointMake([viewLayer bounds].size.width/2.0f, [viewLayer bounds].size.height/2.0f)];
    
    CALayer *translucentLayer = [CALayer layer];
    [translucentLayer setFrame:baseRect];
    [translucentLayer setBackgroundColor:[[UIColor blackColor] CGColor]];
    [translucentLayer setPosition:CGPointMake([viewLayer bounds].size.width/2.0f, [viewLayer bounds].size.height/2.0f)];
    [translucentLayer setOpacity:0.15];
    
    [compositeMaskLayer addSublayer:translucentLayer];
    
    float roll = fabs(attitude.roll) / M_PI_2;
    float pitch = fabs(attitude.pitch) / M_PI_2;
    
    width = 300;// * (1 - 0.7 * roll);
    height = 300 * (1 - 0.7 * pitch);
    
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0.0f, 0.0f, width, height)];
    [circleLayer setBounds:CGRectMake(0.0f, 0.0f, width, height)];
    [circleLayer setPosition:CGPointMake([viewLayer bounds].size.width/2.0f, [viewLayer bounds].size.height/2.0f)];
    [circleLayer setPath:[circlePath CGPath]];
    [circleLayer setFillColor:[[UIColor blackColor] CGColor]];
    
    [compositeMaskLayer addSublayer:circleLayer];
    [self updateSlices];
    
    [viewLayer setMask:compositeMaskLayer];
}

-(void)sliderValueChanged:(id)sender {
    [self updateSlices];
}

-(void)updateSlices {
    int count = round(slider.value);
    [sliceLayer removeFromSuperlayer];
    if (count == 1) return;
    CGFloat centerX = imagePreview.bounds.size.width / 2 + imagePreview.frame.origin.x
    ;
    CGFloat centerY = imagePreview.bounds.size.height / 2 + imagePreview.frame.origin.y;
    
    CGFloat alpha = 2 * M_PI / count;
    
    CGFloat localYaw = yaw;
    
    sliceLayer = [CAShapeLayer layer];
    for (int i = 0; i < count; i++) {
        CGFloat x = (width / 2) * cos(alpha * i + localYaw) + centerX;
        CGFloat y = (height / 2) * sin(alpha * i + localYaw) + centerY;
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(centerX, centerY)];
        [path addLineToPoint:CGPointMake(x, y)];
        
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        shapeLayer.path = [path CGPath];
        shapeLayer.strokeColor = [[UIColor whiteColor] CGColor];
        shapeLayer.lineWidth = 2.0;
        shapeLayer.fillColor = [[UIColor clearColor] CGColor];
        
        [sliceLayer addSublayer:shapeLayer];
    }
    [self.view.layer addSublayer:sliceLayer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

#pragma mark GADRequest generation

- (GADRequest *)request {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as well as any devices
    // you want to receive test ads.
    request.testDevices = @[@"a02d8da2893db6b21e62b2c4ee06574d"];
    return request;
}

#pragma mark GADBannerViewDelegate implementation

// We've received an ad successfully.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"Received ad successfully");
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
}

@end
