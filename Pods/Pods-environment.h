
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ASValueTrackingSlider
#define COCOAPODS_POD_AVAILABLE_ASValueTrackingSlider
#define COCOAPODS_VERSION_MAJOR_ASValueTrackingSlider 0
#define COCOAPODS_VERSION_MINOR_ASValueTrackingSlider 9
#define COCOAPODS_VERSION_PATCH_ASValueTrackingSlider 4

// FlurrySDK
#define COCOAPODS_POD_AVAILABLE_FlurrySDK
#define COCOAPODS_VERSION_MAJOR_FlurrySDK 5
#define COCOAPODS_VERSION_MINOR_FlurrySDK 2
#define COCOAPODS_VERSION_PATCH_FlurrySDK 0

// FlurrySDK/FlurryAds
#define COCOAPODS_POD_AVAILABLE_FlurrySDK_FlurryAds
#define COCOAPODS_VERSION_MAJOR_FlurrySDK_FlurryAds 5
#define COCOAPODS_VERSION_MINOR_FlurrySDK_FlurryAds 2
#define COCOAPODS_VERSION_PATCH_FlurrySDK_FlurryAds 0

// FlurrySDK/FlurrySDK
#define COCOAPODS_POD_AVAILABLE_FlurrySDK_FlurrySDK
#define COCOAPODS_VERSION_MAJOR_FlurrySDK_FlurrySDK 5
#define COCOAPODS_VERSION_MINOR_FlurrySDK_FlurrySDK 2
#define COCOAPODS_VERSION_PATCH_FlurrySDK_FlurrySDK 0

